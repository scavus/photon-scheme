use crate::runtime::{Addr, SchemeGcObj, SchemeObj, ADDR_NULL};
use std::collections::HashSet;
use std::mem::{self, MaybeUninit};

// 64KiB pages
const PAGESIZE: usize = 65536;
const OBJSIZE: usize = std::mem::size_of::<SchemeGcObj>();
const BITMAPSIZE: usize = PAGESIZE / OBJSIZE;
const BITMAP_CHUNK_BITS: usize = 64;
const BITMAP_CHUNK_BYTES: usize = 8;
const BITMAP_NUMCHUNKS: usize = BITMAPSIZE / BITMAP_CHUNK_BYTES;
const NUMCELLS: usize = PAGESIZE / OBJSIZE;

struct Page {
    bitmap: [u64; BITMAP_NUMCHUNKS],
    alloc_bitmap: [u64; BITMAP_NUMCHUNKS],
    heap: [SchemeGcObj; NUMCELLS],
}

impl Page {
    // NOTE: Unsafe
    fn new() -> Self {
        let heap_raw: [MaybeUninit<SchemeGcObj>; NUMCELLS] =
            unsafe { MaybeUninit::uninit().assume_init() };
        let heap = unsafe { mem::transmute::<_, [SchemeGcObj; NUMCELLS]>(heap_raw) };
        Page {
            bitmap: [0; BITMAP_NUMCHUNKS],
            alloc_bitmap: [0; BITMAP_NUMCHUNKS],
            heap,
        }
    }
}

pub struct GcStats {
    allocated_bytes: usize,
    used_slots: usize,
    total_slots: usize,
}

pub struct Gc {
    addr_free: Addr,
    addr_limit: Addr,
    pages: Vec<Page>,
}

impl Gc {
    pub fn new() -> Self {
        Gc {
            addr_free: Addr(0),
            addr_limit: Addr(NUMCELLS),
            pages: vec![Page::new()],
        }
    }

    // TODO:
    // pub fn stats(&mut self) -> GcStats {}

    pub fn addr_free(&self) -> Addr {
        self.addr_free
    }

    pub fn collect(&mut self, roots: &Vec<Addr>) {
        self.mark(roots);
        self.sweep();
        self.compact();
    }

    pub fn load(&self, addr: Addr) -> &SchemeGcObj {
        &self.pages[addr.pageno()].heap[addr.offset()]
    }

    pub fn store(&mut self, addr: Addr, obj: SchemeGcObj) {
        self.pages[addr.pageno()].heap[addr.offset()] = obj;
    }

    pub fn alloc(&mut self, obj: SchemeGcObj) -> Addr {
        let mut addr = ADDR_NULL;
        if self.addr_free < self.addr_limit {
            self.pages[self.addr_free.pageno()].heap[self.addr_free.offset()] = obj;
            addr = self.addr_free;
        } else {
            self.pages.push(Page::new());
            self.addr_free += Addr(1);
            self.addr_limit += Addr(NUMCELLS);
            self.pages[self.addr_free.pageno()].heap[self.addr_free.offset()] = obj;
            addr = self.addr_free;
        }
        self.addr_free += Addr(1);
        self.set_alloc_bit(addr);
        addr
    }

    pub fn free(&mut self, addr: Addr) {
        if addr.is_null() {
            return;
        }
        self.unset_alloc_bit(addr);
    }

    fn get_pointers(&self, obj: &SchemeGcObj) -> Vec<Addr> {
        let mut ptrs = Vec::new();
        match obj {
            SchemeGcObj::ConsCell(x) => {
                if let SchemeObj::Ptr(a) = x.car {
                    if !a.is_null() {
                        ptrs.push(a);
                    }
                }
                if !x.cdr.is_null() {
                    ptrs.push(x.cdr);
                }
            }
            SchemeGcObj::String(x) => {
                if !x.next.is_null() {
                    ptrs.push(x.next);
                }
            }
            SchemeGcObj::Symbol(x) => {
                if !x.next.is_null() {
                    ptrs.push(x.next);
                }
            }
            _ => {}
        }
        ptrs
    }

    // Stop-the-world tri-color bitmapped mark-compact algorithm
    // TODO: Make this incremental.
    fn mark(&mut self, roots: &Vec<Addr>) {
        let mut gray_set = roots.clone();
        for gray_node in &gray_set {
            self.set_mark_bit(*gray_node);
        }
        while let Some(gray_node) = gray_set.pop() {
            let obj = &self.pages[gray_node.pageno()].heap[gray_node.offset()];
            for node in self.get_pointers(obj) {
                if !self.is_mark_bit_set(node) {
                    gray_set.push(node);
                    self.set_mark_bit(node);
                }
            }
        }
    }

    #[inline(always)]
    fn set_mark_bit(&mut self, addr: Addr) {
        let mut bitmap = &mut self.pages[addr.pageno()].bitmap;
        bitmap[addr.offset() / BITMAP_CHUNK_BITS] |= 1 << (addr.offset() % BITMAP_CHUNK_BITS);
    }

    #[inline(always)]
    fn unset_mark_bit(&mut self, addr: Addr) {
        let mut bitmap = &mut self.pages[addr.pageno()].bitmap;
        bitmap[addr.offset() / BITMAP_CHUNK_BITS] ^= 1 << (addr.offset() % BITMAP_CHUNK_BITS);
    }

    #[inline(always)]
    fn is_mark_bit_set(&mut self, addr: Addr) -> bool {
        let bitmap = &mut self.pages[addr.pageno()].bitmap;
        (bitmap[addr.offset() / BITMAP_CHUNK_BITS] & (1 << (addr.offset() % BITMAP_CHUNK_BITS)))
            != 0
    }

    #[inline(always)]
    fn set_alloc_bit(&mut self, addr: Addr) {
        let mut bitmap = &mut self.pages[addr.pageno()].alloc_bitmap;
        bitmap[addr.offset() / BITMAP_CHUNK_BITS] |= 1 << (addr.offset() % BITMAP_CHUNK_BITS);
    }

    #[inline(always)]
    fn unset_alloc_bit(&mut self, addr: Addr) {
        let mut bitmap = &mut self.pages[addr.pageno()].alloc_bitmap;
        bitmap[addr.offset() / BITMAP_CHUNK_BITS] ^= 1 << (addr.offset() % BITMAP_CHUNK_BITS);
    }

    #[inline(always)]
    fn is_alloc_bit_set(&mut self, addr: Addr) -> bool {
        let bitmap = &mut self.pages[addr.pageno()].alloc_bitmap;
        (bitmap[addr.offset() / BITMAP_CHUNK_BITS] & (1 << (addr.offset() % BITMAP_CHUNK_BITS)))
            != 0
    }

    fn sweep(&mut self) {
        for page in &mut self.pages {
            for i in 0..page.bitmap.len() {
                page.alloc_bitmap[i] &= page.bitmap[i];
                page.bitmap[i] = 0;
            }
        }
    }

    // Two-finger compaction
    // NOTE: This may be slow.
    fn compact(&mut self) {
        // Relocate
        let mut free = Addr(0);
        let mut scan = self.addr_free;
        while free < scan {
            while self.is_alloc_bit_set(free) {
                free += Addr(1);
            }
            while !self.is_alloc_bit_set(scan) && scan > free {
                scan -= Addr(1);
            }
            if scan > free {
                self.unset_alloc_bit(scan);
                self.pages[free.pageno()].heap[free.offset()] =
                    self.pages[scan.pageno()].heap[scan.offset()].clone(); // NOTE: Delete clone if copyable
                self.pages[scan.pageno()].heap[scan.offset()] = SchemeGcObj::ForwardAddr(free);
                free += Addr(1);
                scan -= Addr(1);
            }
        }
        self.addr_free = free;
        // Update references
        let mut addr = Addr(0);
        while addr < self.addr_free {
            let mut obj = self.pages[addr.pageno()].heap[addr.offset()].clone();
            match obj {
                SchemeGcObj::ConsCell(mut x) => {
                    if let SchemeObj::Ptr(a) = x.car {
                        if !a.is_null() && a >= self.addr_free {
                            x.car = if let SchemeGcObj::ForwardAddr(fwd) =
                                &self.pages[a.pageno()].heap[a.offset()]
                            {
                                SchemeObj::Ptr(*fwd)
                            } else {
                                SchemeObj::Ptr(ADDR_NULL)
                            }
                        }
                    }
                    if !x.cdr.is_null() && x.cdr >= self.addr_free {
                        x.cdr = if let SchemeGcObj::ForwardAddr(fwd) =
                            &self.pages[x.cdr.pageno()].heap[x.cdr.offset()]
                        {
                            *fwd
                        } else {
                            ADDR_NULL
                        }
                    }
                    obj = SchemeGcObj::ConsCell(x);
                }
                SchemeGcObj::String(mut x) => {
                    if !x.next.is_null() && x.next >= self.addr_free {
                        x.next = if let SchemeGcObj::ForwardAddr(fwd) =
                            &self.pages[x.next.pageno()].heap[x.next.offset()]
                        {
                            *fwd
                        } else {
                            ADDR_NULL
                        };
                        obj = SchemeGcObj::String(x);
                    }
                }
                SchemeGcObj::Symbol(x) => if !x.next.is_null() && x.next >= self.addr_free {},
                _ => {}
            }
            self.pages[addr.pageno()].heap[addr.offset()] = obj;
            addr += Addr(1);
        }
    }
}
