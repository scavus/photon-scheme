use crate::frontend::{SchemeAstNode, SchemeSyntaxError};
use std::iter::Peekable;
use std::mem;
use std::slice::Iter;
use std::string;

#[derive(Debug, PartialEq)]
pub enum SchemeToken {
    Identifier(String),
    Bool(bool),
    Integer(i32),
    Real(f32),
    Character(char),
    String(String),
    OpenParen,
    HashOpenParen,
    CloseParen,
    OpenBracket,
    HashOpenBracket,
    CloseBracket,
    Dot,
    Quote,
    Quasiquote,
    Unquote,
    UnquoteSplicing,
    StringQuotation,
}

pub fn scan(buffer: &String) -> Result<Vec<SchemeToken>, String> {
    let mut tokens = Vec::new();
    let mut it = buffer.chars().peekable();
    while let Some(&c) = it.peek() {
        match c {
            '(' => {
                tokens.push(SchemeToken::OpenParen);
                it.next();
            }
            ')' => {
                tokens.push(SchemeToken::CloseParen);
                it.next();
            }
            '[' => {
                tokens.push(SchemeToken::OpenBracket);
                it.next();
            }
            ']' => {
                tokens.push(SchemeToken::CloseBracket);
                it.next();
            }
            '\'' => {
                tokens.push(SchemeToken::Quote);
                it.next();
            }
            '`' => {
                tokens.push(SchemeToken::Quasiquote);
                it.next();
            }
            ',' => {
                it.next();
                match it.peek() {
                    Some(&c) => {
                        if c == '@' {
                            tokens.push(SchemeToken::UnquoteSplicing);
                            it.next();
                        } else {
                            tokens.push(SchemeToken::Unquote);
                        }
                    }
                    None => {
                        tokens.push(SchemeToken::Unquote);
                    }
                }
            }
            '#' => {
                it.next();
                if let Some(&c) = it.peek() {
                    match c {
                        '\\' => {
                            let mut name = String::new();
                            let mut lookahead_it = it.clone();
                            lookahead_it.next();
                            if let Some(&c) = lookahead_it.peek() {
                                match c {
                                    ' ' | '\n' => break,
                                    _ => name.push(c),
                                }
                            }
                            lookahead_it.next();
                            while let Some(&c) = lookahead_it.peek() {
                                match c {
                                    ' ' | '\n' | ';' | '"' | '(' | ')' | '[' | ']' => break,
                                    _ => name.push(c),
                                }
                                lookahead_it.next();
                            }
                            match name.as_str() {
                                "space" => tokens.push(SchemeToken::Character(' ')),
                                "newline" => tokens.push(SchemeToken::Character('\n')),
                                _ => {
                                    if name.len() == 1 {
                                        tokens.push(SchemeToken::Character(
                                            name.chars().nth(0).unwrap(),
                                        ));
                                    } else {
                                        // TODO: Report a proper error
                                        return Err("Scanner error".to_string());
                                    }
                                }
                            }
                            it = lookahead_it.clone();
                        }
                        '(' => {
                            tokens.push(SchemeToken::HashOpenParen);
                            it.next();
                        }
                        '[' => {
                            tokens.push(SchemeToken::HashOpenBracket);
                            it.next();
                        }
                        _ => {
                            let mut name = String::new();
                            let mut lookahead_it = it.clone();
                            while let Some(&c) = lookahead_it.peek() {
                                match c {
                                    ' ' | '\n' | ';' | '"' | '(' | ')' | '[' | ']' => break,
                                    _ => name.push(c),
                                }
                                lookahead_it.next();
                            }
                            match name.as_str() {
                                "t" | "true" => tokens.push(SchemeToken::Bool(true)),
                                "f" | "false" => tokens.push(SchemeToken::Bool(false)),
                                _ => {
                                    // TODO: Report a proper error
                                    return Err("Scanner error".to_string());
                                }
                            }
                            it = lookahead_it.clone();
                        }
                    }
                } else {
                }
            }
            '\"' => {
                let mut string = String::new();
                let mut lookahead_it = it.clone();
                lookahead_it.next();
                while let Some(&c) = lookahead_it.peek() {
                    match c {
                        '\"' => break,
                        '\\' => {
                            lookahead_it.next();
                            if let Some(&c) = lookahead_it.peek() {
                                match c {
                                    '\"' | '\\' => string.push(c),
                                    // TODO: Report a proper error
                                    _ => return Err("Scanner error".to_string()),
                                }
                            }
                        }
                        _ => string.push(c),
                    }
                    lookahead_it.next();
                }
                lookahead_it.next();
                tokens.push(SchemeToken::String(string));
                it = lookahead_it.clone();
            }
            ';' => {
                it.next();
                while let Some(&c) = it.peek() {
                    match c {
                        '\n' => {
                            break;
                        }
                        _ => {
                            it.next();
                        }
                    }
                }
            }
            ' ' => {
                it.next();
            }
            '\n' => {
                it.next();
            }
            _ => {
                let mut id = String::new();
                id.push(c);
                it.next();
                while let Some(&c) = it.peek() {
                    match c {
                        '(' | ')' | '[' | ']' | '\'' | '`' | ',' | ';' | ' ' | '\n' => {
                            break;
                        }
                        _ => {
                            id.push(c);
                            it.next();
                        }
                    }
                }
                match id.as_str() {
                    "." => tokens.push(SchemeToken::Dot),
                    _ => {
                        if let Ok(n) = id.parse::<i32>() {
                            tokens.push(SchemeToken::Integer(n))
                        } else {
                            if let Ok(n) = id.parse::<f32>() {
                                tokens.push(SchemeToken::Real(n))
                            } else {
                                tokens.push(SchemeToken::Identifier(id.clone()))
                            }
                        }
                    }
                }
            }
        }
    }
    Ok(tokens)
}

/// LL(k) recursive descent parser.
/// Based on R5RS and R6RS specs.
/// Parses a Scheme program in four phases:
/// (1) Datum phase
/// (2) Macro phase
/// (3) Macro expansion phase
/// (4) Expression/Definition phase
pub struct Parser<'a> {
    it: &'a mut Peekable<Iter<'a, SchemeToken>>,
}

impl<'a> Parser<'a> {
    pub fn new(it: &'a mut Peekable<Iter<'a, SchemeToken>>) -> Self {
        Parser { it }
    }

    pub fn run(&mut self) -> Vec<SchemeAstNode> {
        self.program()
    }

    fn expect(&mut self, token: &SchemeToken) -> Result<(), SchemeSyntaxError> {
        if self.it.peek() == Some(&token) {
            self.it.next();
        } else {
            return Err(SchemeSyntaxError::new(format!(
                "Expected {:?}, found {:?}",
                token,
                self.it.peek()
            )));
        }
        Ok(())
    }

    // TODO: Create lists for quote/unquote
    // FIXME: Weird behaviour with dotted lists
    fn datum(&mut self) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut n = SchemeAstNode::Empty;
        let mut it = self.it.clone();
        if let Some(&token) = it.peek() {
            match token {
                SchemeToken::Bool(x) => {
                    self.it.next();
                    n = SchemeAstNode::LiteralBool(*x);
                }
                SchemeToken::Integer(x) => {
                    self.it.next();
                    n = SchemeAstNode::LiteralInteger(*x);
                }
                SchemeToken::Real(x) => {
                    self.it.next();
                    n = SchemeAstNode::LiteralReal(*x);
                }
                SchemeToken::Character(x) => {
                    self.it.next();
                    n = SchemeAstNode::LiteralCharacter(*x);
                }
                SchemeToken::String(x) => {
                    self.it.next();
                    n = SchemeAstNode::LiteralString(x.to_string());
                }
                SchemeToken::Identifier(_) => {
                    n = self.symbol()?;
                }
                SchemeToken::Quote => {
                    self.it.next();
                    let n0 = self.datum()?;
                    n = SchemeAstNode::Quotation(Box::new(n0));
                }
                SchemeToken::Quasiquote => {
                    self.it.next();
                    let n0 = self.datum()?;
                    n = SchemeAstNode::Quasiquotation(Box::new(n0));
                }
                SchemeToken::Unquote => {
                    self.it.next();
                    let n0 = self.datum()?;
                    n = SchemeAstNode::Unquotation(Box::new(n0));
                }
                SchemeToken::UnquoteSplicing => {
                    self.it.next();
                    let n0 = self.datum()?;
                    n = SchemeAstNode::SplicingUnquotation(Box::new(n0));
                }
                SchemeToken::OpenParen => {
                    self.it.next();
                    n = self.datum_list()?;
                    self.expect(&SchemeToken::CloseParen)?;
                }
                SchemeToken::OpenBracket => {
                    self.it.next();
                    n = self.datum_list()?;
                    self.expect(&SchemeToken::CloseBracket)?;
                }
                SchemeToken::HashOpenParen => {
                    self.it.next();
                    n = self.datum_vector()?;
                    self.expect(&SchemeToken::CloseParen)?;
                }
                SchemeToken::HashOpenBracket => {
                    self.it.next();
                    n = self.datum_vector()?;
                    self.expect(&SchemeToken::CloseBracket)?;
                }
                _ => {}
            }
        }
        Ok(SchemeAstNode::Datum(Box::new(n)))
    }

    fn datum_list(&mut self) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut datums = Vec::new();
        let mut rest = SchemeAstNode::Empty;
        while let Some(&token) = self.it.peek() {
            match token {
                SchemeToken::Bool(_)
                | SchemeToken::Integer(_)
                | SchemeToken::Real(_)
                | SchemeToken::Character(_)
                | SchemeToken::String(_)
                | SchemeToken::Identifier(_)
                | SchemeToken::Quote
                | SchemeToken::Quasiquote
                | SchemeToken::Unquote
                | SchemeToken::UnquoteSplicing
                | SchemeToken::OpenParen
                | SchemeToken::OpenBracket
                | SchemeToken::HashOpenParen
                | SchemeToken::HashOpenBracket => {
                    let n = self.datum()?;
                    datums.push(Box::new(n));
                    if Some(&&SchemeToken::Dot) == self.it.peek() {
                        self.it.next();
                        rest = self.datum()?;
                        break;
                    }
                }
                _ => {
                    break;
                }
            }
        }
        Ok(SchemeAstNode::List {
            datums,
            rest: Box::new(rest),
        })
    }

    fn datum_vector(&mut self) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut datums = Vec::new();
        while let Some(&token) = self.it.peek() {
            match token {
                SchemeToken::Bool(_)
                | SchemeToken::Integer(_)
                | SchemeToken::Real(_)
                | SchemeToken::Character(_)
                | SchemeToken::String(_)
                | SchemeToken::Identifier(_)
                | SchemeToken::Quote
                | SchemeToken::Quasiquote
                | SchemeToken::Unquote
                | SchemeToken::UnquoteSplicing
                | SchemeToken::OpenParen
                | SchemeToken::OpenBracket
                | SchemeToken::HashOpenParen
                | SchemeToken::HashOpenBracket => {
                    let n = self.datum()?;
                    println!("{:?}", n.clone());
                    datums.push(Box::new(n));
                }
                _ => {
                    break;
                }
            }
        }
        Ok(SchemeAstNode::Vector { datums })
    }

    fn symbol(&mut self) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let token = self.it.peek().unwrap().clone();
        self.it.next();
        match token {
            SchemeToken::Identifier(x) => {
                return Ok(SchemeAstNode::Identifier(x.to_string()));
            }
            _ => {
                return Err(SchemeSyntaxError::new("Unexpected error".to_string()));
            }
        }
    }

    fn program(&mut self) -> Vec<SchemeAstNode> {
        let mut asts = Vec::new();
        while let Some(&token) = self.it.peek() {
            let node = self.datum().unwrap();
            println!("AST-1 {:?}", node);
            asts.push(node);
        }

        let mut program = Vec::new();
        for ast in asts {
            let form = self.form(&ast).unwrap();
            println!("AST-2 {:?}", form);
            program.push(form);
        }
        program
    }

    fn expression(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut n = SchemeAstNode::Empty;
        if let SchemeAstNode::Datum(datum_node) = node {
            match &**datum_node {
                SchemeAstNode::LiteralBool(_)
                | SchemeAstNode::LiteralInteger(_)
                | SchemeAstNode::LiteralReal(_)
                | SchemeAstNode::LiteralCharacter(_)
                | SchemeAstNode::LiteralString(_) => {
                    n = *datum_node.clone();
                }
                SchemeAstNode::Identifier(id) => match id.as_str() {
                    "define" | "define-syntax" | "quote" | "lambda" | "if" | "set!" => {
                        return Err(SchemeSyntaxError::new(format!(
                            "[expression] Illegal use of keyword <{}>",
                            id
                        )));
                    }
                    _ => {
                        n = *datum_node.clone();
                    }
                },
                SchemeAstNode::List { datums, rest } => {
                    if let SchemeAstNode::Datum(datum) = &*datums[0] {
                        match &**datum {
                            SchemeAstNode::Identifier(id) => match id.as_str() {
                                "quote" => {
                                    // TODO: Error checking
                                    n = SchemeAstNode::Quotation(Box::new(*datums[1].clone()));
                                }
                                "lambda" => {
                                    n = self.lambda_expression(&datum_node)?;
                                }
                                "if" => {
                                    n = self.conditional(&datum_node)?;
                                }
                                "set!" => {
                                    n = self.assignment(&datum_node)?;
                                }
                                _ => {
                                    // TODO: <procedure call> OR <macro use>
                                    n = self.procedure_call(&datum_node)?;
                                }
                            },
                            _ => {
                                n = self.procedure_call(&datum_node)?;
                            }
                        }
                    }
                }
                _ => {}
            }
        }
        Ok(n)
    }

    fn lambda_expression(
        &mut self,
        node: &SchemeAstNode,
    ) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut n0 = SchemeAstNode::Empty;
        let mut n1 = SchemeAstNode::Empty;
        if let SchemeAstNode::List { datums, rest } = node {
            if let SchemeAstNode::Datum(datum_node) = &*datums[1] {
                match &**datum_node {
                    SchemeAstNode::Identifier(_) => {
                        let n0 = *datum_node.clone();
                    }
                    SchemeAstNode::List {
                        datums: sub_datums,
                        rest: sub_rest,
                    } => {
                        let mut variables = Vec::new();
                        let mut rest = SchemeAstNode::Empty;
                        for i in 0..sub_datums.len() {
                            variables.push(Box::new(self.variable(&*sub_datums[i])?));
                        }
                        rest = self.variable(&**sub_rest)?;
                        n0 = SchemeAstNode::Formals {
                            variables,
                            rest: Box::new(rest),
                        }
                    }
                    _ => {}
                }
            }
            {
                // NOTE: Maybe use iterators
                let mut definitions = Vec::new();
                let mut expressions = Vec::new();
                let mut i = 2;
                while i < datums.len() {
                    if self.is_definition(&*datums[i]) {
                        if let SchemeAstNode::Datum(definition_node) = &*datums[i] {
                            definitions.push(Box::new(self.definition(&definition_node)?));
                            i += 1;
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                for j in i..datums.len() {
                    expressions.push(Box::new(self.expression(&*datums[j])?));
                }
                if expressions.len() == 0 {
                    return Err(SchemeSyntaxError::new(
                        "[lambda] Body must contain at least one expression".to_string(),
                    ));
                }
                n1 = SchemeAstNode::Body {
                    definitions,
                    sequence: Box::new(SchemeAstNode::Sequence(expressions)),
                };
            }
        }

        Ok(SchemeAstNode::Lambda {
            formals: Box::new(n0),
            body: Box::new(n1),
        })
    }

    fn conditional(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        if let SchemeAstNode::List { datums, rest } = node {
            if datums.len() != 4 {
                return Err(SchemeSyntaxError::new(
                    "[conditional] <if> expression must contain three expressions".to_string(),
                ));
            }
            let n0 = self.expression(&datums[1])?;
            let n1 = self.expression(&datums[2])?;
            let n2 = self.expression(&datums[3])?;
            return Ok(SchemeAstNode::ConditionalIf {
                test: Box::new(n0),
                consequent: Box::new(n1),
                alternate: Box::new(n2),
            });
        }
        Ok(SchemeAstNode::Empty)
    }

    fn assignment(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        if let SchemeAstNode::List { datums, rest } = node {
            if datums.len() != 3 {
                return Err(SchemeSyntaxError::new(
                    "[assignment] <set!> expression must contain two expressions".to_string(),
                ));
            }
            let n0 = self.variable(&datums[1])?;
            let n1 = self.expression(&datums[2])?;
            return Ok(SchemeAstNode::Assignment {
                variable: Box::new(n0),
                expression: Box::new(n1),
            });
        }
        Ok(SchemeAstNode::Empty)
    }

    fn variable(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        if let SchemeAstNode::Datum(datum_node) = node {
            match &**datum_node {
                SchemeAstNode::Identifier(_) => {
                    return Ok(*datum_node.clone());
                }
                _ => {
                    return Err(SchemeSyntaxError::new(
                        "[variable] Expected <identifier>".to_string(),
                    ));
                }
            }
        }
        Ok(SchemeAstNode::Empty)
    }

    fn procedure_call(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut n0 = SchemeAstNode::Empty;
        let mut operands = Vec::new();
        if let SchemeAstNode::List { datums, rest } = node {
            n0 = self.expression(&datums[0])?;
            for i in 1..datums.len() {
                let n1 = self.expression(&datums[i])?;
                operands.push(Box::new(n1));
            }
        }
        Ok(SchemeAstNode::ProcedureCall {
            operator: Box::new(n0),
            operands,
        })
    }

    fn definition(&mut self, node: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        if let SchemeAstNode::List { datums, rest } = node {
            if let SchemeAstNode::Datum(datum_node) = &*datums[1] {
                match &**datum_node {
                    SchemeAstNode::Identifier(_) => {
                        if datums.len() > 3 {
                            return Err(SchemeSyntaxError::new(
                                "[definition] Multiple expressions after <variable>".to_string(),
                            ));
                        }
                        let n0 = *datum_node.clone();
                        let n1 = self.expression(&*datums[2])?;
                        return Ok(SchemeAstNode::DefinitionVariable {
                            variable: Box::new(n0),
                            expression: Box::new(n1),
                        });
                    }
                    SchemeAstNode::List {
                        datums: sub_datums,
                        rest: sub_rest,
                    } => {
                        let mut n0 = SchemeAstNode::Empty;
                        let mut n1 = SchemeAstNode::Empty;
                        let mut n2 = SchemeAstNode::Empty;
                        {
                            n0 = self.variable(&*sub_datums[0])?;
                        }
                        {
                            let mut variables = Vec::new();
                            let mut rest = SchemeAstNode::Empty;
                            for i in 1..sub_datums.len() {
                                variables.push(Box::new(self.variable(&*sub_datums[i])?));
                            }
                            rest = self.variable(&**sub_rest)?;
                            n1 = SchemeAstNode::Formals {
                                variables,
                                rest: Box::new(rest),
                            }
                        }
                        {
                            // NOTE: Maybe use iterators
                            let mut definitions = Vec::new();
                            let mut expressions = Vec::new();
                            let mut i = 2;
                            while i < datums.len() {
                                if self.is_definition(&*datums[i]) {
                                    if let SchemeAstNode::Datum(definition_node) = &*datums[i] {
                                        definitions
                                            .push(Box::new(self.definition(&definition_node)?));
                                        i += 1;
                                    } else {
                                        break;
                                    }
                                } else {
                                    break;
                                }
                            }
                            for j in i..datums.len() {
                                expressions.push(Box::new(self.expression(&*datums[j])?));
                            }
                            if expressions.len() == 0 {
                                return Err(SchemeSyntaxError::new(
                                    "[definition] Body must contain at least one expression"
                                        .to_string(),
                                ));
                            }
                            n2 = SchemeAstNode::Body {
                                definitions,
                                sequence: Box::new(SchemeAstNode::Sequence(expressions)),
                            };
                        }
                        return Ok(SchemeAstNode::DefinitionProcedure {
                            variable: Box::new(n0),
                            formals: Box::new(n1),
                            body: Box::new(n2),
                        });
                    }
                    _ => {}
                }
            }
        }
        Ok(SchemeAstNode::Empty)
    }

    fn syntax_definition(
        &mut self,
        node: &SchemeAstNode,
    ) -> Result<SchemeAstNode, SchemeSyntaxError> {
        if let SchemeAstNode::List { datums, rest } = node {
            let n0 = self.variable(&datums[1])?;
            let n1 = SchemeAstNode::Empty; // TODO: self.transformer_spec(&datums[2])?;
            return Ok(SchemeAstNode::DefinitionSyntax {
                keyword: Box::new(n0),
                transformer_spec: Box::new(n1),
            });
        }
        Ok(SchemeAstNode::Empty)
    }

    fn transformer_spec(
        &mut self,
        node: &SchemeAstNode,
    ) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut identifiers = Vec::new();
        let mut rules = Vec::new();
        Ok(SchemeAstNode::TransformerSpec { identifiers, rules })
    }

    fn form(&mut self, ast: &SchemeAstNode) -> Result<SchemeAstNode, SchemeSyntaxError> {
        let mut n = SchemeAstNode::Empty;
        if let SchemeAstNode::Datum(toplevel_datum_node) = ast {
            match &**toplevel_datum_node {
                SchemeAstNode::LiteralBool(_)
                | SchemeAstNode::LiteralInteger(_)
                | SchemeAstNode::LiteralReal(_)
                | SchemeAstNode::LiteralCharacter(_)
                | SchemeAstNode::LiteralString(_)
                | SchemeAstNode::Identifier(_) => {
                    n = self.expression(ast)?;
                }
                SchemeAstNode::List { datums, rest } => {
                    if let SchemeAstNode::Datum(datum_node) = &*datums[0] {
                        match &**datum_node {
                            SchemeAstNode::Identifier(id) => match id.as_str() {
                                "define" => {
                                    n = self.definition(toplevel_datum_node)?;
                                }
                                "define-syntax" => {
                                    n = self.syntax_definition(toplevel_datum_node)?;
                                }
                                _ => {
                                    n = self.expression(ast)?;
                                }
                            },
                            _ => {
                                n = self.expression(ast)?;
                            }
                        }
                    }
                }
                _ => {
                    return Err(SchemeSyntaxError::new(
                        "[form] Unexpected error".to_string(),
                    ));
                }
            }
        }
        Ok(n)
    }

    fn is_definition(&mut self, node: &SchemeAstNode) -> bool {
        if let SchemeAstNode::Datum(datum_node) = node {
            if let SchemeAstNode::List { datums, rest } = &**datum_node {
                if let SchemeAstNode::Datum(datum_node) = &*datums[0] {
                    if let SchemeAstNode::Identifier(id) = &**datum_node {
                        if id == "define" {
                            return true;
                        }
                    }
                }
            }
        }
        false
    }
}
