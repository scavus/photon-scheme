use std::ops::{Add, AddAssign, Sub, SubAssign};
// NOTE: 64bit only
/// 53 bit page number, 11 bit offset
#[derive(Debug, Copy, Clone, Eq, PartialEq, PartialOrd, Hash)]
pub struct Addr(pub usize);
const PAGENO_BITS: usize = 53;
const OFFSET_BITS: usize = 11;
pub const ADDR_NULL: Addr = Addr(usize::MAX);

impl Addr {
    #[inline(always)]
    pub fn pageno(&self) -> usize {
        (self.0 >> OFFSET_BITS) as usize
    }

    #[inline(always)]
    pub fn offset(&self) -> usize {
        (self.0 & 0b00000_00000000_00000000_00000000_00000000_00000000_00000000_111_1111_1111)
            as usize
    }

    #[inline(always)]
    pub fn is_null(&self) -> bool {
        self.0 == usize::MAX
    }
}

impl Add for Addr {
    type Output = Self;

    #[inline(always)]
    fn add(self, other: Self) -> Self {
        Self(self.0 + other.0)
    }
}

impl AddAssign for Addr {
    #[inline(always)]
    fn add_assign(&mut self, other: Self) {
        *self = Self(self.0 + other.0);
    }
}

impl Sub for Addr {
    type Output = Self;

    #[inline(always)]
    fn sub(self, other: Self) -> Self {
        Self(self.0 - other.0)
    }
}

impl SubAssign for Addr {
    #[inline(always)]
    fn sub_assign(&mut self, other: Self) {
        *self = Self(self.0 - other.0);
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SchemeObj {
    Empty,
    Bool(bool),
    Integer(i32),
    Real(f32),
    Char(char),
    Ptr(Addr),
}

impl SchemeObj {
    #[inline(always)]
    pub fn from_bool(x: bool) -> Self {
        SchemeObj::Bool(x)
    }

    // pub fn to_bool() -> bool {}

    #[inline(always)]
    pub fn from_i32(x: i32) -> Self {
        SchemeObj::Integer(x)
    }

    // pub fn to_i32() -> i32 {}

    #[inline(always)]
    pub fn from_f32(x: f32) -> Self {
        SchemeObj::Real(x)
    }

    // pub fn to_f32() -> f32 {}

    #[inline(always)]
    pub fn from_char(x: char) -> Self {
        SchemeObj::Char(x)
    }

    // pub fn to_char() -> char {}

    //#[inline(always)]
    //pub fn from_ptr() -> Self {}

    // pub fn to_ptr() -> Addr {}
}

#[derive(Debug, Copy, Clone)]
pub struct SchemeConsCell {
    pub car: SchemeObj,
    pub cdr: Addr,
}

#[derive(Debug, Copy, Clone)]
pub struct SchemeFn {
    pub addr: u32,
}

#[derive(Debug, Copy, Clone)]
pub struct SchemeString {
    pub data: char,
    pub next: Addr,
}

#[derive(Debug, Copy, Clone)]
pub struct SchemeSymbol {
    pub data: char,
    pub next: Addr,
}

#[derive(Debug, Clone)]
pub enum SchemeGcObj {
    Empty,
    ForwardAddr(Addr),
    ConsCell(SchemeConsCell),
    Vector(Vec<SchemeObj>),
    Sfn(SchemeFn),
    Rfn(fn(Vec<SchemeObj>) -> SchemeObj),
    String(SchemeString),
    Symbol(SchemeSymbol),
    BoxedString(String),
    BoxedSymbol(String),
}

pub mod base;
pub mod gc;
pub mod vm;
