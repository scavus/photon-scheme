use crate::frontend::SchemeAstNode;
use std::collections::{HashMap, HashSet};

#[derive(Debug, Copy, Clone)]
pub enum TacOpcode {
    Load,
    Store,
    Call,
    Jump,
    Branch,
    Return,
    Mkfn,
    Param,
    Input,
    Define,
}

#[derive(Debug, Clone, PartialEq)]
pub enum TacOperand {
    Empty,
    Tmp(usize),
    Symbol(String),
    Label(usize),
    NumArgs(usize),
    ConstBool(bool),
    ConstInteger(i32),
    ConstReal(f32),
    ConstChar(char),
    ConstString(String),
}

#[derive(Debug)]
pub struct Tac(
    pub TacOpcode,
    pub TacOperand,
    pub TacOperand,
    pub TacOperand,
);

pub struct BasicBlock {
    pub label: usize,
    pub level: usize,
    pub ilist: Vec<Tac>,
    pub conns: Vec<usize>,
}

impl BasicBlock {
    fn new(label: usize, level: usize) -> Self {
        BasicBlock {
            label,
            level,
            ilist: Vec::new(),
            conns: Vec::new(),
        }
    }
}

pub enum SymbolKind {
    Variable,
    Procedure,
}

pub struct SymbolAttribs {
    kind: SymbolKind,
}

pub struct Level {
    pub symtab: HashMap<String, u64>,
    pub up: i64,
}

impl Level {
    fn new(up: i64) -> Self {
        Level {
            symtab: HashMap::new(),
            up,
        }
    }
}

pub struct IrBuilder {
    pub blocks: Vec<BasicBlock>,
    pub levels: Vec<Level>,
    active_block_idx: usize,
    tmp_counter: u64,
    label_counter: usize,
}

/// IR builder.
/// Takes the AST representation of the program and emits TAC IR in basic blocks.
impl IrBuilder {
    pub fn new() -> Self {
        IrBuilder {
            blocks: Vec::new(),
            levels: Vec::new(),
            active_block_idx: 0,
            tmp_counter: 0,
            label_counter: 0,
        }
    }

    fn create_tmp(&mut self, r: TacOperand) -> TacOperand {
        let t = if r == TacOperand::Empty {
            let t = TacOperand::Tmp(self.tmp_counter as usize);
            self.tmp_counter += 1;
            t
        } else {
            r.clone()
        };
        t
    }

    fn create_label(&mut self) -> usize {
        let l = self.label_counter;
        self.label_counter += 1;
        l
    }

    fn create_block(&mut self, level: usize) -> usize {
        let label = self.create_label();
        self.blocks.push(BasicBlock::new(label, level));
        self.blocks.len() - 1
    }

    fn create_level(&mut self, up: i64) -> usize {
        self.levels.push(Level::new(up));
        self.levels.len() - 1
    }

    pub fn build(&mut self, program: &Vec<SchemeAstNode>) {
        let toplevel_env_idx = self.create_level(-1);
        let block_idx = self.create_block(toplevel_env_idx);
        self.active_block_idx = block_idx;
        for root in program {
            match root {
                SchemeAstNode::DefinitionVariable {
                    variable: _,
                    expression: _,
                } => {
                    self.build_definition(&root, toplevel_env_idx);
                }
                _ => {
                    self.build_expression(&root, toplevel_env_idx, TacOperand::Empty);
                }
            }
        }
    }

    // TODO: Function definition
    fn build_definition(&mut self, node: &SchemeAstNode, env_idx: usize) {
        if let SchemeAstNode::DefinitionVariable {
            variable: v,
            expression: e,
        } = node
        {
            if let SchemeAstNode::Identifier(x) = &**v {
                let t1 = TacOperand::Symbol(x.to_string());
                let t2 = self.build_expression(&*e, env_idx, TacOperand::Empty);
                self.blocks[self.active_block_idx].ilist.push(Tac(
                    TacOpcode::Define,
                    t1,
                    t2,
                    TacOperand::Empty,
                ));
            }
        }
    }

    fn build_expression(
        &mut self,
        node: &SchemeAstNode,
        env_idx: usize,
        r: TacOperand,
    ) -> TacOperand {
        match node {
            SchemeAstNode::Identifier(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::Symbol(x.to_string());
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::Symbol(x.to_string()),
                        TacOperand::Empty,
                    ));
                    return TacOperand::Symbol(x.to_string());
                }
            }
            SchemeAstNode::LiteralBool(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::ConstBool(*x);
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::ConstBool(*x),
                        TacOperand::Empty,
                    ));
                    return TacOperand::ConstBool(*x);
                }
            }
            SchemeAstNode::LiteralInteger(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::ConstInteger(*x);
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::ConstInteger(*x),
                        TacOperand::Empty,
                    ));
                    return TacOperand::ConstInteger(*x);
                }
            }
            SchemeAstNode::LiteralReal(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::ConstReal(*x);
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::ConstReal(*x),
                        TacOperand::Empty,
                    ));
                    return TacOperand::ConstReal(*x);
                }
            }
            SchemeAstNode::LiteralCharacter(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::ConstChar(*x);
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::ConstChar(*x),
                        TacOperand::Empty,
                    ));
                    return TacOperand::ConstChar(*x);
                }
            }
            SchemeAstNode::LiteralString(x) => {
                if r == TacOperand::Empty {
                    return TacOperand::ConstString(x.to_string());
                } else {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Store,
                        r,
                        TacOperand::ConstString(x.to_string()),
                        TacOperand::Empty,
                    ));
                    return TacOperand::ConstString(x.to_string());
                }
            }
            SchemeAstNode::Quotation(datum) => {
                if let SchemeAstNode::Datum(d) = &**datum {
                    match &**d {
                        SchemeAstNode::List { datums, rest } => {}
                        SchemeAstNode::Vector { datums } => {}
                        _ => {}
                    }
                    return TacOperand::Empty;
                } else {
                    return TacOperand::Empty;
                }
            }
            SchemeAstNode::Sequence(s) => {
                let mut result = TacOperand::Empty;
                for expr in s {
                    result = self.build_expression(&*expr, env_idx, TacOperand::Empty);
                }
                return result;
            }
            SchemeAstNode::Lambda {
                formals: f,
                body: b,
            } => {
                let lambda_env_idx = self.create_level(env_idx as i64);
                let lambda_block_idx = self.create_block(lambda_env_idx);
                let prev_block_idx = self.active_block_idx;
                self.active_block_idx = lambda_block_idx;
                let mut num_args = 0;
                if let SchemeAstNode::Formals { variables, rest } = &**f {
                    for arg in variables {
                        let t = self.build_expression(&*arg, lambda_env_idx, TacOperand::Empty);
                        self.blocks[self.active_block_idx].ilist.push(Tac(
                            TacOpcode::Input,
                            t,
                            TacOperand::Empty,
                            TacOperand::Empty,
                        ));
                    }
                    num_args = variables.len();
                }
                let mut result = TacOperand::Empty;
                if let SchemeAstNode::Body {
                    definitions,
                    sequence,
                } = &**b
                {
                    for def in definitions {
                        self.build_definition(&*def, lambda_env_idx);
                    }
                    result = self.build_expression(&*sequence, lambda_env_idx, TacOperand::Empty);
                }
                self.blocks[self.active_block_idx].ilist.push(Tac(
                    TacOpcode::Return,
                    result,
                    TacOperand::Empty,
                    TacOperand::Empty,
                ));
                self.active_block_idx = prev_block_idx;
                let result = self.create_tmp(r);
                let label = self.blocks[lambda_block_idx].label;
                self.blocks[self.active_block_idx].ilist.push(Tac(
                    TacOpcode::Mkfn,
                    TacOperand::Label(label),
                    TacOperand::NumArgs(num_args),
                    result.clone(),
                ));
                return result;
            }
            SchemeAstNode::ConditionalIf {
                test: t,
                consequent: c,
                alternate: a,
            } => {
                let t1 = self.build_expression(&*t, env_idx, TacOperand::Empty);
                let t_br_env_idx = self.create_level(env_idx as i64);
                let f_br_env_idx = self.create_level(env_idx as i64);
                let exit_block_idx = self.create_block(env_idx);
                let t_br_block_idx = self.create_block(t_br_env_idx);
                let f_br_block_idx = self.create_block(f_br_env_idx);
                let exit_label = TacOperand::Label(self.blocks[exit_block_idx].label);
                let t_label = TacOperand::Label(self.blocks[t_br_block_idx].label);
                let f_label = TacOperand::Label(self.blocks[f_br_block_idx].label);
                self.blocks[self.active_block_idx].ilist.push(Tac(
                    TacOpcode::Branch,
                    t1,
                    t_label,
                    f_label,
                ));
                let result = self.create_tmp(r);
                self.active_block_idx = t_br_block_idx;
                self.build_expression(&*c, t_br_env_idx, result.clone());
                self.blocks[t_br_block_idx].ilist.push(Tac(
                    TacOpcode::Jump,
                    exit_label.clone(),
                    TacOperand::Empty,
                    TacOperand::Empty,
                ));
                self.active_block_idx = f_br_block_idx;
                self.build_expression(&*a, f_br_env_idx, result.clone());
                self.blocks[f_br_block_idx].ilist.push(Tac(
                    TacOpcode::Jump,
                    exit_label.clone(),
                    TacOperand::Empty,
                    TacOperand::Empty,
                ));
                self.active_block_idx = exit_block_idx;
                return result;
            }
            SchemeAstNode::Assignment {
                variable: v,
                expression: e,
            } => {
                let t1 = self.build_expression(&*v, env_idx, TacOperand::Empty);
                let t2 = self.build_expression(&*e, env_idx, TacOperand::Empty);
                let result = t1.clone();
                self.blocks[self.active_block_idx].ilist.push(Tac(
                    TacOpcode::Store,
                    t1,
                    t2,
                    TacOperand::Empty,
                ));
                return result;
            }
            SchemeAstNode::ProcedureCall {
                operator: f,
                operands: args,
            } => {
                let t1 = self.build_expression(&*f, env_idx, TacOperand::Empty);
                let mut params = Vec::new();
                for arg in args.iter().rev() {
                    params.push(self.build_expression(&*arg, env_idx, TacOperand::Empty));
                }
                for param in params {
                    self.blocks[self.active_block_idx].ilist.push(Tac(
                        TacOpcode::Param,
                        param,
                        TacOperand::Empty,
                        TacOperand::Empty,
                    ));
                }
                let n = TacOperand::NumArgs(args.len());
                let t2 = self.create_tmp(r);
                let result = t2.clone();
                self.blocks[self.active_block_idx]
                    .ilist
                    .push(Tac(TacOpcode::Call, t1, n, t2));
                return result;
            }
            _ => {
                return TacOperand::Empty;
            }
        }
    }

    pub fn print(&self) {
        for block in &self.blocks {
            println!("┌―");
            println!("│ L{}:", block.label);
            for tac in &block.ilist {
                println!("│ {:?}", tac);
            }
            println!("└―");
        }
    }

    pub fn pretty_print(&self) {
        fn operand_to_string(operand: &TacOperand) -> String {
            match operand {
                TacOperand::Tmp(x) => {
                    let mut s = "$".to_string();
                    s.push_str(&x.to_string());
                    return s;
                }
                TacOperand::Symbol(x) => {
                    let mut s = "%".to_string();
                    s.push_str(&x);
                    return s;
                }
                TacOperand::Label(x) => {
                    let mut s = "L".to_string();
                    s.push_str(&x.to_string());
                    return s;
                }
                TacOperand::ConstBool(x) => {
                    return x.to_string();
                }
                TacOperand::ConstInteger(x) => {
                    return x.to_string();
                }
                TacOperand::ConstReal(x) => {
                    return x.to_string();
                }
                TacOperand::NumArgs(x) => {
                    return x.to_string();
                }
                _ => {
                    return "".to_string();
                }
            }
        }
        for block in &self.blocks {
            println!("┌―");
            println!("│ L{}:", block.label);
            for tac in &block.ilist {
                let Tac(opcode, operand0, operand1, operand2) = tac;
                let mut s = String::new();
                match opcode {
                    TacOpcode::Load => {
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(" := ");
                        s.push_str(&operand_to_string(operand1));
                    }
                    TacOpcode::Store => {
                        s.push_str("*");
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(" := ");
                        s.push_str(&operand_to_string(operand1));
                    }
                    TacOpcode::Call => {
                        s.push_str(&operand_to_string(operand2));
                        s.push_str(" := ");
                        s.push_str("call ");
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(" ");
                        s.push_str(&operand_to_string(operand1));
                    }
                    TacOpcode::Jump => {
                        s.push_str("jump ");
                        s.push_str(&operand_to_string(operand0));
                    }
                    TacOpcode::Branch => {
                        s.push_str("branch ");
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(" ");
                        s.push_str(&operand_to_string(operand1));
                        s.push_str(" ");
                        s.push_str(&operand_to_string(operand2));
                    }
                    TacOpcode::Return => {
                        s.push_str("ret ");
                        s.push_str(&operand_to_string(operand0));
                    }
                    TacOpcode::Mkfn => {
                        s.push_str(&operand_to_string(operand2));
                        s.push_str(" := ");
                        s.push_str("λ");
                        s.push_str("(");
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(",");
                        s.push_str(&operand_to_string(operand1));
                        s.push_str(")");
                    }
                    TacOpcode::Param => {
                        s.push_str("param ");
                        s.push_str(&operand_to_string(operand0));
                    }
                    TacOpcode::Input => {
                        s.push_str("in ");
                        s.push_str(&operand_to_string(operand0));
                    }
                    TacOpcode::Define => {
                        s.push_str("define ");
                        s.push_str(&operand_to_string(operand0));
                        s.push_str(" ");
                        s.push_str(&operand_to_string(operand1));
                    }
                }
                println!("│ {}", s);
            }
            println!("└―");
        }
    }
}
