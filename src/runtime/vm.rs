use crate::codegen::bytecode::{self, Bytecode, DataSectionEntry};
use crate::runtime::gc::Gc;
use crate::runtime::{Addr, SchemeConsCell, SchemeFn, SchemeGcObj, SchemeObj, ADDR_NULL};
use crate::runtime::base;
use std::collections::HashMap;

pub struct Vm {
    pub gc: Gc,
    bc: Bytecode,
    ip: usize,
    regfile: Vec<SchemeObj>,
    param_stack: Vec<SchemeObj>,
    activation_stack: Vec<(usize, u64, Vec<(u64, SchemeObj)>)>,
    function_table: HashMap<String, fn(&mut Vm, &Vec<SchemeObj>) -> SchemeObj>,
}

impl Vm {
    pub fn new(bc: Bytecode) -> Self {
        let mut function_table: HashMap<String, fn(&mut Vm, &Vec<SchemeObj>) -> SchemeObj> =
            HashMap::new();
        function_table.insert("+".to_string(), base::add);
        function_table.insert("-".to_string(), base::sub);
        function_table.insert("*".to_string(), base::mul);
        function_table.insert("/".to_string(), base::div);
        function_table.insert(">".to_string(), base::gt);
        function_table.insert(">=".to_string(), base::gte);
        function_table.insert("<".to_string(), base::lt);
        function_table.insert("<=".to_string(), base::lte);
        function_table.insert("=".to_string(), base::eq);
        function_table.insert("list".to_string(), base::list);
        function_table.insert("car".to_string(), base::car);
        function_table.insert("cdr".to_string(), base::cdr);
        function_table.insert("null?".to_string(), base::is_null);
        function_table.insert("println".to_string(), base::println);
        function_table.insert("display".to_string(), base::display);
        function_table.insert("newline".to_string(), base::newline);
        function_table.insert("integer?".to_string(), base::is_integer);
        function_table.insert("real?".to_string(), base::is_real);
        Vm {
            gc: Gc::new(),
            bc,
            ip: 0,
            regfile: vec![SchemeObj::Empty; 128],
            param_stack: Vec::new(),
            activation_stack: Vec::new(),
            function_table,
        }
    }

    fn rootset(&self) -> Vec<Addr> {
        let mut rootset = Vec::new();
        for obj in &self.regfile {
            if let SchemeObj::Ptr(addr) = obj {
                if !rootset.contains(addr) {
                    rootset.push(*addr);
                }
            }
        }
        for obj in &self.param_stack {
            if let SchemeObj::Ptr(addr) = obj {
                if !rootset.contains(addr) {
                    rootset.push(*addr);
                }
            }
        }
        rootset
    }

    // TODO: Move this to GC
    fn update_rootset(&mut self) {
        for obj in &mut self.regfile {
            if let SchemeObj::Ptr(addr) = obj {
                if !addr.is_null() && addr >= &mut self.gc.addr_free() {
                    if let SchemeGcObj::ForwardAddr(fwd) = &self.gc.load(*addr) {
                        *obj = SchemeObj::Ptr(*fwd)
                    } else {
                        // *obj = SchemeObj::Ptr(ADDR_NULL)
                    }
                }
            }
        }
        for obj in &mut self.param_stack {
            if let SchemeObj::Ptr(addr) = obj {
                if !addr.is_null() && addr >= &mut self.gc.addr_free() {
                    if let SchemeGcObj::ForwardAddr(fwd) = &self.gc.load(*addr) {
                        *obj = SchemeObj::Ptr(*fwd)
                    } else {
                        // *obj = SchemeObj::Ptr(ADDR_NULL)
                    }
                }
            }
        }
    }

    pub fn run(&mut self) {
        let mut args = Vec::new();
        args.reserve(32);
        let mut cycle = 0;
        loop {
            if cycle > 5000 {
                let free = self.gc.addr_free();
                // println!(
                //     "[debug] [free] pageno: {}, offset: {}",
                //     free.pageno(),
                //     free.offset()
                // );
                // let t_start = Instant::now();
                self.gc.collect(&self.rootset());
                // println!("[info] [gc] Took {} ms", t_start.elapsed().as_millis());
                self.update_rootset();
                cycle = 0;
            }
            cycle += 1;
            let instr = self.bc.text[self.ip];
            match instr.opcode() {
                bytecode::OP_LDC => {
                    let r0 = instr.r0();
                    let r1 = instr.r1();
                    match &self.bc.data[r1 as usize] {
                        DataSectionEntry::Symbol(x) => {
                            self.regfile[r0 as usize] =
                                SchemeObj::Ptr(self.gc.alloc(SchemeGcObj::BoxedSymbol(x.clone())));
                        }
                        DataSectionEntry::String(x) => {
                            self.regfile[r0 as usize] =
                                SchemeObj::Ptr(self.gc.alloc(SchemeGcObj::BoxedString(x.clone())));
                        }
                        DataSectionEntry::Bool(x) => {
                            self.regfile[r0 as usize] = SchemeObj::Bool(*x);
                        }
                        DataSectionEntry::Integer(x) => {
                            self.regfile[r0 as usize] = SchemeObj::Integer(*x);
                        }
                        DataSectionEntry::Real(x) => {
                            self.regfile[r0 as usize] = SchemeObj::Real(*x);
                        }
                        DataSectionEntry::Char(x) => {
                            self.regfile[r0 as usize] = SchemeObj::Char(*x);
                        }
                    }
                }
                bytecode::OP_ST => {
                    let r0 = instr.r0();
                    let r1 = instr.r1();
                    self.regfile[r0 as usize] = self.regfile[r1 as usize];
                }
                // FIXME: Activation stack isn't prepared correctly.
                bytecode::OP_CALL => {
                    let r0 = instr.r0();
                    let r1 = instr.r1();
                    let r2 = instr.r2();
                    if let SchemeObj::Ptr(addr) = self.regfile[r0 as usize] {
                        let obj = self.gc.load(addr);
                        match obj {
                            SchemeGcObj::Sfn(f) => {
                                self.activation_stack.push((self.ip + 1, r2, Vec::new()));
                                self.ip = f.addr as usize;
                                continue;
                            }
                            SchemeGcObj::BoxedSymbol(s) => {
                                // println!("[debug] procedure {}", s);
                                for n in 0..r1 {
                                    let arg = self.param_stack.pop().unwrap();
                                    // println!("[debug] arg {}: {:?}", n, arg);
                                    args.push(arg);
                                    // args[n] = arg;
                                }
                                match s.as_str() {
                                    "-" => self.regfile[r2 as usize] = base::sub(self, &args),
                                    "*" => self.regfile[r2 as usize] = base::mul(self, &args),
                                    ">" => self.regfile[r2 as usize] = base::gt(self, &args),
                                    _ => {
                                        self.regfile[r2 as usize] =
                                            self.function_table[s](self, &args)
                                    }
                                }
                                args.clear();
                                // println!("[debug] return {:?}", self.regfile[r2 as usize]);
                            }
                            _ => {
                                println!("[error][runtime] Not a function: {:?}", obj);
                                break;
                            }
                        }
                    } else {
                        println!("[error][runtime] Not a function");
                        break;
                    }
                }
                bytecode::OP_JUMP => {
                    let r0 = instr.r0();
                    self.ip = r0 as usize;
                    continue;
                }
                bytecode::OP_BR => {
                    let r0 = instr.r0();
                    let r1 = instr.r1();
                    let r2 = instr.r2();
                    if self.regfile[r0 as usize] == SchemeObj::Bool(false) {
                        self.ip = r2 as usize;
                    } else {
                        self.ip = r1 as usize;
                    }
                    continue;
                }
                bytecode::OP_MKFN => {
                    let r0 = instr.r0();
                    let r1 = instr.r1();
                    let r2 = instr.r2();
                    self.regfile[r2 as usize] = SchemeObj::Ptr(
                        self.gc
                            .alloc(SchemeGcObj::Sfn(SchemeFn { addr: r0 as u32 })),
                    );
                }
                bytecode::OP_RET => {
                    let r0 = instr.r0();
                    let ar = self.activation_stack.pop();
                    match ar {
                        Some(record) => {
                            self.ip = record.0;
                            let tmp = self.regfile[r0 as usize];
                            // self.regfile = record.2;
                            for (r, v) in record.2 {
                                self.regfile[r as usize] = v;
                            }
                            self.regfile[record.1 as usize] = tmp;
                            continue;
                        }
                        None => {
                            break;
                        }
                    }
                }
                bytecode::OP_PUSH => {
                    let r0 = instr.r0();
                    self.param_stack.push(self.regfile[r0 as usize]);
                    // println!("[debug] param_stack: {:?}", self.param_stack);
                }
                bytecode::OP_POP => {
                    let r0 = instr.r0();
                    self.regfile[r0 as usize] = self.param_stack.pop().unwrap();
                    let end = self.activation_stack.len() - 1;
                    self.activation_stack[end]
                        .2
                        .push((r0, self.regfile[r0 as usize]));
                }
                _ => {}
            }
            self.ip += 1;
        }
    }
}
