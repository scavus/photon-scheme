use std::error::Error;
use std::fmt;

#[derive(Debug, Clone)]
pub enum SchemeAstNode {
    // Datum:
    Datum(Box<SchemeAstNode>),
    List {
        datums: Vec<Box<SchemeAstNode>>,
        rest: Box<SchemeAstNode>,
    },
    Vector {
        datums: Vec<Box<SchemeAstNode>>,
    },
    Quotation(Box<SchemeAstNode>),
    Quasiquotation(Box<SchemeAstNode>),
    Unquotation(Box<SchemeAstNode>),
    SplicingUnquotation(Box<SchemeAstNode>),

    // Macros:
    TransformerSpec {
        identifiers: Vec<Box<SchemeAstNode>>,
        rules: Vec<Box<SchemeAstNode>>,
    },

    // Definitions:
    DefinitionVariable {
        variable: Box<SchemeAstNode>,
        expression: Box<SchemeAstNode>,
    },
    DefinitionProcedure {
        variable: Box<SchemeAstNode>,
        formals: Box<SchemeAstNode>,
        body: Box<SchemeAstNode>,
    },
    DefinitionSyntax {
        keyword: Box<SchemeAstNode>,
        transformer_spec: Box<SchemeAstNode>,
    },

    // Expressions:
    Identifier(String),
    LiteralBool(bool),
    LiteralNumber(String),
    LiteralInteger(i32),
    LiteralReal(f32),
    LiteralCharacter(char),
    LiteralString(String),
    Lambda {
        formals: Box<SchemeAstNode>,
        body: Box<SchemeAstNode>,
    },
    ConditionalIf {
        test: Box<SchemeAstNode>,
        consequent: Box<SchemeAstNode>,
        alternate: Box<SchemeAstNode>,
    },
    Assignment {
        variable: Box<SchemeAstNode>,
        expression: Box<SchemeAstNode>,
    },
    ProcedureCall {
        operator: Box<SchemeAstNode>,
        operands: Vec<Box<SchemeAstNode>>,
    },
    MacroUse {
        keyword: Box<SchemeAstNode>,
        datums: Vec<Box<SchemeAstNode>>,
    },

    // Auxilliary:
    Formals {
        variables: Vec<Box<SchemeAstNode>>,
        rest: Box<SchemeAstNode>,
    },
    Body {
        definitions: Vec<Box<SchemeAstNode>>,
        sequence: Box<SchemeAstNode>,
    },
    Sequence(Vec<Box<SchemeAstNode>>),
    Empty,
}

#[derive(Debug)]
pub struct SchemeSyntaxError {
    message: String,
}

impl SchemeSyntaxError {
    fn new(message: String) -> Self {
        SchemeSyntaxError { message }
    }
}

impl fmt::Display for SchemeSyntaxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Syntax error {}", self.message)
    }
}

impl Error for SchemeSyntaxError {}

pub mod parser;
