use crate::ir::{BasicBlock, IrBuilder, Level, Tac, TacOpcode, TacOperand};
use std::collections::{HashMap, HashSet};
use std::ops::{Add, AddAssign, Sub, SubAssign};

// Bytecode specification
#[derive(Copy, Clone)]
pub struct Instr(pub u64);
const INSTR_BITS: usize = 64;
const INSTR_BYTES: usize = 8;
pub const OP_LDC: u8 = 0;
pub const OP_LD: u8 = 1;
pub const OP_ST: u8 = 2;
pub const OP_PUSH: u8 = 3;
pub const OP_POP: u8 = 4;
pub const OP_CALL: u8 = 5;
pub const OP_TCALL: u8 = 6;
pub const OP_JUMP: u8 = 7;
pub const OP_BR: u8 = 8;
pub const OP_MKFN: u8 = 9;
pub const OP_RET: u8 = 10;

impl Instr {
    #[inline(always)]
    pub fn opcode(&self) -> u8 {
        (self.0 & 0b000000000000000000000000000000000000000000000000000000000_1111111u64) as u8
    }

    #[inline(always)]
    pub fn r0(&self) -> u64 {
        ((self.0 >> 7) & 0b000000000000000000000000000000000000000000000_1111111111111111111u64)
    }

    #[inline(always)]
    pub fn r1(&self) -> u64 {
        ((self.0 >> 26) & 0b000000000000000000000000000000000000000000000_1111111111111111111u64)
    }

    #[inline(always)]
    pub fn r2(&self) -> u64 {
        ((self.0 >> 45) & 0b000000000000000000000000000000000000000000000_1111111111111111111u64)
    }

    #[inline(always)]
    fn emit(opcode: u8, r0: u64, r1: u64, r2: u64) -> Self {
        Instr(opcode as u64 | r0 << 7 | r1 << 26 | r2 << 45)
    }
}

#[derive(Debug, PartialEq)]
pub enum DataSectionEntry {
    Symbol(String),
    String(String),
    Bool(bool),
    Integer(i32),
    Real(f32),
    Char(char),
}

pub struct Bytecode {
    pub text: Vec<Instr>,
    pub data: Vec<DataSectionEntry>,
}

struct Env {
    symtab: HashMap<String, u64>,
    up: i64,
}

impl Env {
    fn new(up: i64) -> Self {
        Env {
            symtab: HashMap::new(),
            up,
        }
    }
}

/// Code generator for register-based VM
pub struct BytecodeBuilder {
    register_desc: Vec<HashSet<String>>,
    addr_desc: Vec<Env>,
    register_freelist: Vec<u64>,
    label_map: HashMap<u64, u64>,
}

impl BytecodeBuilder {
    pub fn new() -> Self {
        BytecodeBuilder {
            register_desc: Vec::new(),
            addr_desc: Vec::new(),
            register_freelist: Vec::new(),
            label_map: HashMap::new(),
        }
    }

    fn addr_desc_lookup(&self, level: usize, key: &String) -> Option<&u64> {
        let mut current_level = level as i64;
        let mut up_level = self.addr_desc[level].up;
        let mut result = None;
        loop {
            result = self.addr_desc[current_level as usize].symtab.get(key);
            if result == None {
                current_level = up_level;
                if current_level == -1 {
                    break;
                }
                up_level = self.addr_desc[current_level as usize].up;
            } else {
                return result;
            }
        }
        result
    }

    fn addr_desc_insert(&mut self, level: usize, key: &String, val: u64) {
        self.addr_desc[level].symtab.insert(key.clone(), val);
    }

    fn next_reg(&mut self) -> u64 {
        if self.register_freelist.is_empty() {
            self.register_desc.push(HashSet::new());
            return (self.register_desc.len() - 1) as u64;
        } else {
            return self.register_freelist.pop().unwrap();
        }
    }

    /// Get a register but only lookup to symbol table at 'level'.
    /// This is needed for definitions that obscure previous definitions.
    fn get_dst_reg_single_level(&mut self, level: usize, operand: &TacOperand) -> u64 {
        match operand {
            TacOperand::Symbol(x) => {
                let key = x.to_string();
                match self.addr_desc[level].symtab.get(&key) {
                    Some(v) => *v,
                    None => {
                        let r = self.next_reg();
                        self.register_desc[r as usize].insert(key.clone());
                        self.addr_desc_insert(level, &key, r);
                        r
                    }
                }
            }
            _ => 0,
        }
    }

    fn get_dst_reg(&mut self, level: usize, operand: &TacOperand) -> u64 {
        match operand {
            TacOperand::Symbol(x) => {
                let key = x.to_string();
                match self.addr_desc_lookup(level, &key) {
                    Some(v) => *v,
                    None => {
                        let r = self.next_reg();
                        self.register_desc[r as usize].insert(key.clone());
                        self.addr_desc_insert(level, &key, r);
                        r
                    }
                }
            }
            TacOperand::Tmp(x) => {
                let key = x.to_string();
                match self.addr_desc_lookup(level, &key) {
                    Some(v) => *v,
                    None => {
                        let r = self.next_reg();
                        self.register_desc[r as usize].insert(key.clone());
                        self.addr_desc_insert(level, &key, r);
                        r
                    }
                }
            }
            _ => 0,
        }
    }

    fn get_src_reg(
        &mut self,
        level: usize,
        operand: &TacOperand,
        text: &mut Vec<Instr>,
        data: &mut Vec<DataSectionEntry>,
    ) -> u64 {
        match operand {
            TacOperand::Symbol(x) => {
                let key = x.to_string();
                match self.addr_desc_lookup(level, &key) {
                    Some(v) => *v,
                    None => {
                        let entry = DataSectionEntry::Symbol(key.clone());
                        // TODO:
                        // data.contains(&entry);
                        data.push(entry);
                        let r0 = self.next_reg();
                        let r1 = (data.len() - 1) as u64;
                        text.push(Instr::emit(OP_LDC, r0, r1, 0));
                        // FIXME: Scope problems
                        self.register_desc[r0 as usize].insert(key.clone());
                        self.addr_desc_insert(level, &key, r0);
                        r0
                    }
                }
            }
            TacOperand::Tmp(x) => {
                let key = x.to_string();
                match self.addr_desc_lookup(level, &key) {
                    Some(v) => *v,
                    None => {
                        let r = self.next_reg();
                        self.register_desc[r as usize].insert(key.clone());
                        self.addr_desc_insert(level, &key, r);
                        r
                    }
                }
            }
            TacOperand::ConstBool(x) => {
                data.push(DataSectionEntry::Bool(*x));
                let r0 = self.next_reg();
                let r1 = (data.len() - 1) as u64;
                text.push(Instr::emit(OP_LDC, r0, r1, 0));
                r0
            }
            TacOperand::ConstInteger(x) => {
                data.push(DataSectionEntry::Integer(*x));
                let r0 = self.next_reg();
                let r1 = (data.len() - 1) as u64;
                text.push(Instr::emit(OP_LDC, r0, r1, 0));
                r0
            }
            TacOperand::ConstReal(x) => {
                data.push(DataSectionEntry::Real(*x));
                let r0 = self.next_reg();
                let r1 = (data.len() - 1) as u64;
                text.push(Instr::emit(OP_LDC, r0, r1, 0));
                r0
            }
            TacOperand::ConstChar(x) => {
                data.push(DataSectionEntry::Char(*x));
                let r0 = self.next_reg();
                let r1 = (data.len() - 1) as u64;
                text.push(Instr::emit(OP_LDC, r0, r1, 0));
                r0
            }
            TacOperand::ConstString(x) => {
                data.push(DataSectionEntry::String(x.to_string()));
                let r0 = self.next_reg();
                let r1 = (data.len() - 1) as u64;
                text.push(Instr::emit(OP_LDC, r0, r1, 0));
                r0
            }
            _ => 0,
        }
    }

    pub fn build(&mut self, ir: &IrBuilder) -> Bytecode {
        let mut text: Vec<Instr> = Vec::new();
        let mut data: Vec<DataSectionEntry> = Vec::new();
        self.addr_desc.reserve(ir.levels.len());
        for level in &ir.levels {
            self.addr_desc.push(Env::new(level.up));
        }
        for (block_idx, block) in ir.blocks.iter().enumerate() {
            self.label_map.insert(block.label as u64, text.len() as u64);
            for tac in &block.ilist {
                let Tac(opcode, operand0, operand1, operand2) = tac;
                match opcode {
                    TacOpcode::Load => {
                        let r0 = self.get_dst_reg(block.level, &operand0);
                        let r1 = self.get_src_reg(block.level, &operand1, &mut text, &mut data);
                        text.push(Instr::emit(OP_LD, r0, r1, 0));
                    }
                    TacOpcode::Store => {
                        let r0 = self.get_dst_reg(block.level, &operand0);
                        let r1 = self.get_src_reg(block.level, &operand1, &mut text, &mut data);
                        text.push(Instr::emit(OP_ST, r0, r1, 0));
                    }
                    TacOpcode::Call => {
                        let r0 = self.get_src_reg(block.level, &operand0, &mut text, &mut data);
                        let r1 = match operand1 {
                            TacOperand::NumArgs(n) => *n as u64,
                            _ => 0u64,
                        };
                        let r2 = self.get_dst_reg(block.level, &operand2);
                        text.push(Instr::emit(OP_CALL, r0, r1, r2));
                    }
                    TacOpcode::Jump => {
                        let r0 = match operand0 {
                            TacOperand::Label(x) => *x as u64,
                            _ => 0,
                        };
                        text.push(Instr::emit(OP_JUMP, r0, 0, 0));
                    }
                    TacOpcode::Branch => {
                        let r0 = self.get_src_reg(block.level, &operand0, &mut text, &mut data);
                        let r1 = match operand1 {
                            TacOperand::Label(x) => *x as u64,
                            _ => 0,
                        };
                        let r2 = match operand2 {
                            TacOperand::Label(x) => *x as u64,
                            _ => 0,
                        };
                        text.push(Instr::emit(OP_BR, r0, r1, r2));
                    }
                    TacOpcode::Return => {
                        let r0 = self.get_src_reg(block.level, &operand0, &mut text, &mut data);
                        text.push(Instr::emit(OP_RET, r0, 0, 0));
                    }
                    TacOpcode::Mkfn => {
                        let r0 = match operand0 {
                            TacOperand::Label(x) => *x as u64,
                            _ => 0,
                        };
                        let r1 = match operand1 {
                            TacOperand::NumArgs(n) => *n as u64,
                            _ => 0u64,
                        };
                        let r2 = self.get_dst_reg(block.level, &operand2);
                        text.push(Instr::emit(OP_MKFN, r0, r1, r2));
                    }
                    TacOpcode::Param => {
                        let r0 = self.get_src_reg(block.level, &operand0, &mut text, &mut data);
                        text.push(Instr::emit(OP_PUSH, r0, 0, 0));
                    }
                    TacOpcode::Input => {
                        if let TacOperand::Symbol(x) = operand0 {
                            let key = x.to_string();
                            let r0 = self.next_reg();
                            self.register_desc[r0 as usize].insert(key.clone());
                            self.addr_desc_insert(block.level, &key, r0);
                            text.push(Instr::emit(OP_POP, r0, 0, 0));
                        }
                    }
                    TacOpcode::Define => {
                        let r0 = self.get_dst_reg_single_level(block.level, &operand0);
                        let r1 = self.get_src_reg(block.level, &operand1, &mut text, &mut data);
                        text.push(Instr::emit(OP_ST, r0, r1, 0));
                    }
                }
            }
            if block_idx == 0 {
                text.push(Instr::emit(OP_RET, 0, 0, 0));
            }
            // TODO: Reclaim registers used in this block
        }
        for i in 0..text.len() {
            let instr = text[i];
            match instr.opcode() {
                OP_JUMP => {
                    let r0 = self.label_map[&instr.r0()];
                    text[i] = Instr::emit(instr.opcode(), r0, instr.r1(), instr.r2());
                }
                OP_BR => {
                    let r1 = self.label_map[&instr.r1()];
                    let r2 = self.label_map[&instr.r2()];
                    text[i] = Instr::emit(instr.opcode(), instr.r0(), r1, r2);
                }
                OP_MKFN => {
                    let r0 = self.label_map[&instr.r0()];
                    text[i] = Instr::emit(instr.opcode(), r0, instr.r1(), instr.r2());
                }
                _ => {}
            }
        }
        Bytecode { text, data }
    }
}

// Converts bytecode to text
pub fn to_text(bc: &Bytecode) -> String {
    let mut s = String::new();
    s.push_str(".text");
    s.push_str("\n");
    for (l, instr) in bc.text.iter().enumerate() {
        s.push_str(&l.to_string());
        s.push_str(": ");
        match instr.opcode() {
            OP_LDC => {
                s.push_str("LDC ");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str("@");
                s.push_str(&instr.r1().to_string());
                s.push_str("\n");
            }
            OP_LD => {
                s.push_str("LD ");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str(&instr.r1().to_string());
                s.push_str("\n");
            }
            OP_ST => {
                s.push_str("ST ");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str(&instr.r1().to_string());
                s.push_str("\n");
            }
            OP_CALL => {
                s.push_str("CALL ");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str(&instr.r1().to_string());
                s.push_str(" ");
                s.push_str(&instr.r2().to_string());
                s.push_str("\n");
            }
            OP_JUMP => {
                s.push_str("JUMP ");
                s.push_str("@");
                s.push_str(&instr.r0().to_string());
                s.push_str("\n");
            }
            OP_BR => {
                s.push_str("BR ");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str("@");
                s.push_str(&instr.r1().to_string());
                s.push_str(" ");
                s.push_str("@");
                s.push_str(&instr.r2().to_string());
                s.push_str("\n");
            }
            OP_MKFN => {
                s.push_str("MKFN ");
                s.push_str("@");
                s.push_str(&instr.r0().to_string());
                s.push_str(" ");
                s.push_str(&instr.r1().to_string());
                s.push_str(" ");
                s.push_str(&instr.r2().to_string());
                s.push_str("\n");
            }
            OP_RET => {
                s.push_str("RET ");
                s.push_str(&instr.r0().to_string());
                s.push_str("\n");
            }
            OP_PUSH => {
                s.push_str("PUSH ");
                s.push_str(&instr.r0().to_string());
                s.push_str("\n");
            }
            OP_POP => {
                s.push_str("POP ");
                s.push_str(&instr.r0().to_string());
                s.push_str("\n");
            }
            _ => {}
        }
    }
    s.push_str(".data");
    s.push_str("\n");
    for (l, entry) in bc.data.iter().enumerate() {
        s.push_str(&l.to_string());
        s.push_str(": ");
        s.push_str(&format!("{:?}", entry).to_string());
        s.push_str("\n");
    }
    s
}
