use crate::runtime::{Addr, SchemeGcObj, SchemeObj, SchemeConsCell, ADDR_NULL};
use crate::vm::Vm;

/// Base library procedures
pub fn add(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = 0f32;
    if args.len() == 0 {
        return SchemeObj::Real(result);
    }
    for arg in args {
        if let SchemeObj::Real(x) = arg {
            result += x;
        } else {
            return SchemeObj::Real(result);
        }
    }
    SchemeObj::Real(result)
}

pub fn sub(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = 0f32;
    if args.len() == 0 {
        return SchemeObj::Real(result);
    } else if args.len() == 1 {
        if let SchemeObj::Real(x) = args[0] {
            result = -x;
        } else {
            return SchemeObj::Real(result);
        }
    } else {
        if let SchemeObj::Real(x) = args[0] {
            result = x;
        } else {
            return SchemeObj::Real(result);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result -= x;
            } else {
                return SchemeObj::Real(result);
            }
        }
    }
    SchemeObj::Real(result)
}

pub fn mul(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = 1f32;
    if args.len() == 0 {
        return SchemeObj::Real(result);
    }
    for arg in args {
        if let SchemeObj::Real(x) = arg {
            result *= x;
        } else {
            return SchemeObj::Real(result);
        }
    }
    SchemeObj::Real(result)
}

pub fn div(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = 1f32;
    if args.len() == 0 {
        return SchemeObj::Real(result);
    }
    if let SchemeObj::Real(x) = args[0] {
        result = x;
    } else {
        return SchemeObj::Real(result);
    }
    for i in 1..args.len() {
        if let SchemeObj::Real(x) = args[i] {
            result /= x;
        } else {
            return SchemeObj::Real(result);
        }
    }
    SchemeObj::Real(result)
}

pub fn gt(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = true;
    if args.len() <= 1 {
        return SchemeObj::Bool(true);
    } else {
        let mut prev;
        if let SchemeObj::Real(x) = args[0] {
            prev = x;
        } else {
            return SchemeObj::Bool(false);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result = result && (prev > x);
                prev = x;
            } else {
                return SchemeObj::Bool(false);
            }
        }
    }
    SchemeObj::Bool(result)
}

pub fn gte(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = true;
    if args.len() <= 1 {
        return SchemeObj::Bool(true);
    } else {
        let mut prev;
        if let SchemeObj::Real(x) = args[0] {
            prev = x;
        } else {
            return SchemeObj::Bool(false);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result = result && (prev >= x);
                prev = x;
            } else {
                return SchemeObj::Bool(false);
            }
        }
    }
    SchemeObj::Bool(result)
}

pub fn lt(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = true;
    if args.len() <= 1 {
        return SchemeObj::Bool(true);
    } else {
        let mut prev;
        if let SchemeObj::Real(x) = args[0] {
            prev = x;
        } else {
            return SchemeObj::Bool(false);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result = result && (prev < x);
                prev = x;
            } else {
                return SchemeObj::Bool(false);
            }
        }
    }
    SchemeObj::Bool(result)
}

pub fn lte(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = true;
    if args.len() <= 1 {
        return SchemeObj::Bool(true);
    } else {
        let mut prev;
        if let SchemeObj::Real(x) = args[0] {
            prev = x;
        } else {
            return SchemeObj::Bool(false);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result = result && (prev <= x);
                prev = x;
            } else {
                return SchemeObj::Bool(false);
            }
        }
    }
    SchemeObj::Bool(result)
}

pub fn eq(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut result = true;
    if args.len() <= 1 {
        return SchemeObj::Bool(true);
    } else {
        let mut prev;
        if let SchemeObj::Real(x) = args[0] {
            prev = x;
        } else {
            return SchemeObj::Bool(false);
        }
        for i in 1..args.len() {
            if let SchemeObj::Real(x) = args[i] {
                result = result && (prev == x);
                prev = x;
            } else {
                return SchemeObj::Bool(false);
            }
        }
    }
    SchemeObj::Bool(result)
}

pub fn list(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    let mut addrs = Vec::new();
    for i in 0..args.len() {
        let addr = vm.gc.alloc(SchemeGcObj::Empty);
        addrs.push(addr);
    }
    addrs.push(ADDR_NULL);
    for i in 0..args.len() {
        let obj = SchemeConsCell {
            car: args[i],
            cdr: addrs[i + 1],
        };
        vm.gc.store(addrs[i], SchemeGcObj::ConsCell(obj));
    }
    SchemeObj::Ptr(addrs[0])
}

pub fn car(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    }
    if let SchemeObj::Ptr(addr) = args[0] {
        let obj = vm.gc.load(addr);
        match obj {
            SchemeGcObj::ConsCell(cell) => match cell {
                SchemeConsCell { car, cdr } => {
                    return car.clone();
                }
                _ => {
                    return SchemeObj::Empty;
                }
            },
            _ => {
                return SchemeObj::Empty;
            }
        }
    } else {
        return SchemeObj::Empty;
    }
}

pub fn cdr(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    }
    if let SchemeObj::Ptr(addr) = args[0] {
        let obj = vm.gc.load(addr);
        match obj {
            SchemeGcObj::ConsCell(cell) => match cell {
                SchemeConsCell { car, cdr } => {
                    return SchemeObj::Ptr(*cdr);
                }
                _ => {
                    return SchemeObj::Empty;
                }
            },
            _ => {
                return SchemeObj::Empty;
            }
        }
    } else {
        return SchemeObj::Empty;
    }
}

pub fn is_null(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    }
    if args[0] == SchemeObj::Ptr(ADDR_NULL) {
        return SchemeObj::Bool(true);
    } else {
        return SchemeObj::Bool(false);
    }
}

pub fn println(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    }
    match args[0] {
        SchemeObj::Ptr(addr) => {
            let obj = vm.gc.load(addr);
            println!("{:?}", obj);
        }
        _ => {
            println!("{:?}", args[0]);
        }
    }
    SchemeObj::Empty
}

pub fn display(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    }
    match args[0] {
        SchemeObj::Ptr(addr) => {
            let obj = vm.gc.load(addr);
            print!("{:?}", obj);
        }
        _ => {
            print!("{:?}", args[0]);
        }
    }
    SchemeObj::Empty
}

pub fn newline(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() > 0 {
        return SchemeObj::Empty;
    }
    print!("\n");
    SchemeObj::Empty
}

pub fn is_integer(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    } else {
        match args[0] {
            SchemeObj::Integer(_) => {
                return SchemeObj::Bool(true);
            }
            _ => {
                return SchemeObj::Bool(false);
            }
        }
    }
}

pub fn is_real(vm: &mut Vm, args: &Vec<SchemeObj>) -> SchemeObj {
    if args.len() != 1 {
        return SchemeObj::Empty;
    } else {
        match args[0] {
            SchemeObj::Real(_) => {
                return SchemeObj::Bool(true);
            }
            _ => {
                return SchemeObj::Bool(false);
            }
        }
    }
}
